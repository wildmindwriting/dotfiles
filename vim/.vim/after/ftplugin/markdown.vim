setlocal spell
setlocal spelllang=en_us
autocmd vimenter * Goyo | SoftPencil
" autocmd vimenter * MarkdownPreview
set nocursorline
let g:gruvbox_italic=1
let g:gruvbox_bold=1
set conceallevel=2
set background=light

" Undo points should be different
" See: https://jonathanh.co.uk/blog/writing-prose-in-vim/
inoremap ! !<C-g>u
inoremap , ,<C-g>u
inoremap . .<C-g>u
inoremap : :<C-g>u
inoremap ; ;<C-g>u
inoremap ? ?<C-g>u
inoremap ( <C-g>u(
inoremap ) )<C-g>u
