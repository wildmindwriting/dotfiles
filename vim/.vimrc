" Don't need to be compatible with vi
set nocompatible
filetype off
filetype plugin indent on
filetype plugin on

" Color syntactically?
syntax enable
syntax on
set foldmethod=indent
set foldnestmax=10
set nofoldenable
set foldlevel=2

colorscheme deus
let g:gruvbox_italic=1
let g:gruvbox_bold=1
set conceallevel=2
set background=dark

set t_Co=256
if (has("termguicolors"))
  set termguicolors
endif

" NERDTree
let g:NERDTreeWinSize=60

" Remap leader
let mapleader=","

" No bells
set noerrorbells visualbell t_vb=
autocmd GUIEnter * set visualbell t_vb=

" Encoding
set encoding=utf-8

" Show hybrid line numbers on side
set number relativenumber

" Cursorline
set cursorline

" Ledger
" See: https://github.com/ledger/vim-ledger
au BufNewFile,BufRead *.ledger set filetype=ledger
let g:ledger_maxwidth = 80
let g:ledger_align_at = 70

" Set split below
set splitbelow

" Font
set guifont=IosevkaTerm\ NFM\ Light
highlight Comment cterm=italic

" Change how the bufferline filenames are displayed. Want the full path
" relative to the current directory
let g:bufferline_fname_mod = ':.'

" For autocompletion
set wildmode=list:longest

" Search
set incsearch
set showmatch
set hlsearch
set ignorecase

" Remove highlighting after search
nnoremap ,<space> :noh<cr>

" Handle long lines correctly
set wrap
set linebreak

" Spacing/Tabs
set expandtab
set tabstop=2
set shiftwidth=4
set softtabstop=4
set smartindent
set smarttab
set autoindent

" KEY MAPPINGS
" Disallow the use of arrow keys
" Normal mode
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
" Insert mode:
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>

" Move between split buffers
nnoremap <C-h> <c-w>h
nnoremap <C-j> <c-w>j
nnoremap <C-k> <c-w>k
nnoremap <C-l> <c-w>l

" Move out of insert mode
inoremap ,e <Esc>

" Map Nerdtree
map <C-n> :NERDTreeToggle<CR>

" Save files:
set backupdir=~/.vim/backup//
set directory=~/.vim/swap//
set undodir=~/.vim/undo//
set backupcopy=yes

" Status line
set laststatus=2

function! GitBranch()
  return system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
endfunction

function! StatuslineGit()
  let l:branchname = GitBranch()
  return strlen(l:branchname) > 0?'  '.l:branchname.' ':''
endfunction

set statusline=
set statusline+=%#PmenuSel#
set statusline+=%{StatuslineGit()}
set statusline+=%#LineNr#
set statusline+=\ %f
set statusline+=%m
set statusline+=%=
set statusline+=%#CursorColumn#
set statusline+=\ %y
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
set statusline+=\ [%{&fileformat}\]
set statusline+=\ %p%%
set statusline+=\ %l:%c
set statusline+=

" Highlighting brackets bothers me
let g:loaded_matchparen=0

" Standards for git commit messages
autocmd Filetype gitcommit setlocal spell textwidth=72
