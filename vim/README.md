# Installation

1. Remove the `~/.vimrc` file and `~/.vim` directory.
2. Clone this repo and move the contents of the `vim` directory to your home directory, which is usually `/home/<username>/`.
3. Run the following commands:
```
git -C ~/.vim/pack/vendor/start clone git@github.com:preservim/nerdtree.git
git -C ~/.vim/pack/vendor/start clone git@github.com:airblade/vim-gitgutter.git
git -C ~/.vim/pack/vendor/start clone git@github.com:bling/vim-bufferline.git
git -C ~/.vim/pack/vendor/start clone git@github.com:junegunn/goyo.vim.git
git -C ~/.vim/pack/vendor/start clone git@github.com:reedes/vim-pencil.git
git -C ~/.vim/pack/vendor/start clone git@github.com:ledger/vim-ledger.git
git -C ~/.vim/pack/vendor/start clone git@github.com:preservim/vim-markdown.git
git -C ~/.vim/pack/vendor/start clone git@github.com:iamcco/markdown-preview.nvim.git
```
4. After install `markdown-preview.nvim`, you should probably execute:
```
$ bash ~/.vim/pack/vendor/start/markdown-preview.nvim/app/install.sh
```
5. Prosper!

# Resources

- [How to manage Vim plugins natively](https://linuxconfig.org/how-to-manage-vim-plugins-natively)
- [writing vim](https://github.com/phantomdiorama/writingvim)
- [CommonMark](https://commonmark.org/)
