# ZSH & ohmyzsh

_Note:_ As of June 2024, I'm slowly rethinking how I use `zsh` and may move back to plain ol' `bash` again.

- [Install and set up `zsh` as default](https://github.com/ohmyzsh/ohmyzsh/wiki/Installing-ZSH)
- [Follow instructions to set up `oh-my-zsh`](https://github.com/ohmyzsh/ohmyzsh)
